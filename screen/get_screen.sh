#!/bin/sh
wget http://git.savannah.gnu.org/cgit/screen.git/snapshot/v.4.3.1.tar.gz
tar -xvf v.4.3.1.tar.gz
cd v.4.3.1/src/

#Build GNU Screen

./autogen.sh
./configure
make
make install

#Download Custom Screen Profile
wget "https://gitlab.com/josh.davis.sdf/yinsen/-/raw/master/screen/.screenrc"
mv .screenrc ~/